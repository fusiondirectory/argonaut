#!/usr/bin/perl

########################################################################
#
#  argonaut-user-reminder
#
#  Check for expired users and send them a mail allowing to postpone expiration
#
#  This code is part of FusionDirectory (http://www.fusiondirectory.org/)
#  Copyright (C) 2015-2018  FusionDirectory
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA.
#
########################################################################

use strict;
use warnings;
use 5.008;

use Digest::SHA qw(sha256_base64);

use Argonaut::Libraries::Common qw(:ldap :file :string :config);

use Argonaut::Libraries::WorkflowUpdate qw(argonaut_supann_update_states);

use Log::Handler;

use Net::LDAP::Constant qw(LDAP_NO_SUCH_OBJECT);
use Net::LDAP::Util qw(generalizedTime_to_time);

use Mail::Sendmail qw(sendmail);
use MIME::Base64;
use MIME::Words qw(encode_mimewords);

use DateTime;

my $config;

$config = argonaut_read_config;
$config->{'fd_rdn'} = 'ou=fusiondirectory';

my $logdir;

eval {
  my $client_settings = argonaut_get_client_settings($config,$config->{'client_ip'});
  $logdir = $client_settings->{'logdir'};
};
if ($@) {
  $logdir = '/var/log/argonaut';
}

my $logfile     = "argonaut-user-reminder.log";

my $verbose   = 0;
my $posix     = 0;
my $ppolicy   = 0;
my $supann    = 0;
my $workflow  = 0;

sub print_usage
{
  my ($help) = @_;
  print "Usage : argonaut-user-reminder [--help] [--verbose] [--posix] [--ppolicy] [--supann] [--all] [--supann-update]\n";
  if ($help) {
    print << "EOF";

  --help            : this (help) message
  --verbose         : be verbose
  --posix           : check POSIX account expiration
  --ppolicy         : check ppolicy password expiration
  --supann          : check supannRessourceEtatDate
  --all             : check POSIX, ppolicy and supannRessourceEtatDate
  --supann-update   : maintain supann workflow through webservice
EOF
    exit(0);
  } else {
    exit(-1);
  }
}

foreach my $arg ( @ARGV ) {
  if (lc($arg) eq "--verbose") {
    $verbose = 1;
  } elsif (lc($arg) eq "--posix") {
    $posix    = 1;
  } elsif (lc($arg) eq "--ppolicy") {
    $ppolicy  = 1;
  } elsif (lc($arg) eq "--supann") {
    $supann   = 1;
  } elsif (lc($arg) eq "--all") {
    $posix    = 1;
    $ppolicy  = 1;
    $supann   = 1;
  } elsif (lc($arg) eq "--supann-update") {
    $workflow = 1;
  } elsif ((lc($arg) eq "--help") || (lc($arg) eq "-h")) {
    print_usage(1);
  } else {
    print_usage(0);
  }
}

if (!$posix && !$ppolicy && !$supann && !$workflow) {
  # Defaults to --all
  $posix    = 1;
  $ppolicy  = 1;
  $supann   = 1;
}

argonaut_create_dir($logdir);

our $log = Log::Handler->create_logger("argonaut-user-reminder");

$log->add(
  file => {
    filename => "$logdir/$logfile",
    maxlevel => ($verbose ? "info" : "notice"),
    minlevel => "emergency",
    newline  => 1,
  },
  screen => {
    log_to   => "STDOUT",
    maxlevel => ($verbose ? "info" : "notice"),
    minlevel => "notice",
    newline  => 1,
  },
  screen => {
    log_to   => "STDERR",
    maxlevel => "warning",
    minlevel => "emergency",
    newline  => 1,
  },
);

eval {
  if ($workflow) {
    argonaut_supann_update_states($verbose);
  }

  check_expired_users();
};
if ($@) {
  $log->error("Died with: $@");
  die $@;
}

exit 0;

##########################################################################################

# Die on all LDAP error except for «No such object»
sub die_on_ldap_errors
{
  my ($mesg) = @_;
  if (($mesg->code != 0) && ($mesg->code != LDAP_NO_SUCH_OBJECT)) {
    die $mesg->error;
  }
}

#############################################################

# Read FD config in the LDAP
sub read_reminder_ldap_config
{
  my ($ldap) = @_;

  # Default values
  $config->{'user_rdn'}       = 'ou=people';
  $config->{'token_rdn'}      = 'ou=reminder';
  # Days before expiration to send the first mail
  $config->{'alert_delay'}    = 15;
  # Days after first mail to send a new one
  $config->{'resend_delay'}   = 7;
  # Should alert mails be forwarded to the manager
  $config->{'forward_alert'}    = 1;
  $config->{'forward_ppolicy'}  = 1;
  $config->{'alert_expired'}    = 0;
  $config->{'forward_expired'}  = 1;
  $config->{'use_alternate'}    = 1;

  my $entry = argonaut_read_ldap_config(
    $ldap,
    $config->{'ldap_base'},
    $config,
    '(&(objectClass=fusionDirectoryConf)(objectClass=fdUserReminderPluginConf))',
    {
      'user_rdn'            => "fdUserRDN",
      'token_rdn'           => "fdReminderTokenRDN",
      'alert_delay'         => "fdUserReminderAlertDelay",
      'resend_delay'        => "fdUserReminderResendDelay",
      'alert_mailsubject'   => "fdUserReminderAlertSubject",
      'alert_mailbody'      => "fdUserReminderAlertBody",
      'alert_mailaddress'   => "fdUserReminderEmail",
      'ppolicy_default'     => "fdPpolicyDefaultCn",
      'ppolicy_rdn'         => "fdPpolicyRDN",
      'ppolicy_mailsubject' => "fdUserReminderPpolicyAlertSubject",
      'ppolicy_mailbody'    => "fdUserReminderPpolicyAlertBody",
      'expired_mailsubject' => "fdUserReminderExpirationSubject",
      'expired_mailbody'    => "fdUserReminderExpirationBody",
    }
  );

  if ($entry->exists('fdUserReminderForwardAlert')) {
    $config->{'forward_alert'} = ($entry->get_value('fdUserReminderForwardAlert') eq "TRUE");
  }

  if ($entry->exists('fdUserReminderForwardPpolicyAlert')) {
    $config->{'forward_ppolicy'} = ($entry->get_value('fdUserReminderForwardPpolicyAlert') eq "TRUE");
  }

  if ($entry->exists('fdUserReminderExpiration')) {
    $config->{'alert_expired'} = ($entry->get_value('fdUserReminderExpiration') eq "TRUE");
  }

  if ($entry->exists('fdUserReminderForwardExpiration')) {
    $config->{'forward_expired'} = ($entry->get_value('fdUserReminderForwardExpiration') eq "TRUE");
  }

  if ($entry->exists('fdUserReminderUseAlternate')) {
    $config->{'use_alternate'} = ($entry->get_value('fdUserReminderUseAlternate') eq "TRUE");
  }
}

sub check_expired_users
{
  my ($ldap,$ldap_base) = argonaut_ldap_handle($config);
  $config->{'ldap_base'} = $ldap_base;
  read_reminder_ldap_config($ldap);

  # Time and date in seconds
  my $now = time();

  # Convert alert_delay (days) to seconds and add it to $now (86400 is 24*60*60)
  my $next_alert_date = ($now + ($config->{'alert_delay'} * 86400));

  if ($posix) {
    # POSIX expiration
    my $mesg = $ldap->search(
      base    => $config->{'ldap_base'},
      filter  => '(&(objectClass=person)(shadowExpire=*))',
      scope   => 'subtree'
    );
    die_on_ldap_errors($mesg);

    foreach my $entry ($mesg->entries()) {
      my $cn = $entry->get_value('cn');
      my $shadowExpireSeconds = $entry->get_value('shadowExpire') * 86400;
      if ($shadowExpireSeconds <= $now) {
        $log->info("$cn is Expired");
        alert_user_if_needed($ldap,$entry,$now,'expired');
      } elsif ($shadowExpireSeconds <= $next_alert_date) {
        alert_user_if_needed($ldap,$entry,$now,'posix');
      }
    }
  }

  if ($ppolicy) {
    # PPOLICY expiration
    if (($config->{'ppolicy_default'}) && ($config->{'ppolicy_rdn'})) {
      my $defaultMaxAge;
      my $ppolicydn = 'cn='.$config->{'ppolicy_default'}.','.$config->{'ppolicy_rdn'}.','.$config->{'ldap_base'};
      my $mesg = $ldap->search(
        base    => $ppolicydn,
        filter  => '(objectClass=*)',
        scope   => 'base',
        attrs   => ['pwdMaxAge']
      );
      die_on_ldap_errors($mesg);
      if ($mesg->count > 0) {
        $defaultMaxAge = ($mesg->entries)[0]->get_value("pwdMaxAge");
      } else {
        die "Default ppolicy '".$ppolicydn."' could not be found in the LDAP!\n";
      }

      $mesg = $ldap->search(
        base    => $config->{'ldap_base'},
        filter  => '(&(objectClass=person)(pwdChangedTime=*))',
        scope   => 'subtree',
        attrs   => ['uid','cn','mail','gosaMailAlternateAddress','fdPrivateMail','supannAutreMail','manager','pwdChangedTime','pwdPolicySubentry','pwdAccountLockedTime'],
      );
      die_on_ldap_errors($mesg);

      my %maxAgeCache = ();
      my @entries = $mesg->entries();
      foreach my $entry (@entries) {
        my $cn = $entry->get_value('cn');
        if (not defined $cn) {
          $cn = $entry->dn;
        }

        my $pwdChangedTimestamp = generalizedTime_to_time($entry->get_value('pwdChangedTime'));
        if (not defined $pwdChangedTimestamp) {
          $log->notice("Failed to parse value '".$entry->get_value('pwdChangedTime')."' for $cn");
          next;
        }

        my $maxAge = $defaultMaxAge;
        if (defined $entry->get_value('pwdPolicySubentry')) {
          my $userPolicy = $entry->get_value('pwdPolicySubentry');
          if (defined $maxAgeCache{$userPolicy}) {
            $maxAge = $maxAgeCache{$userPolicy};
          } else {
            $mesg = $ldap->search(
              base    => $userPolicy,
              filter  => '(objectClass=*)',
              scope   => 'base',
              attrs   => ['pwdMaxAge']
            );
            die_on_ldap_errors($mesg);
            if ($mesg->count > 0) {
              $maxAge = ($mesg->entries)[0]->get_value("pwdMaxAge");
            }
            $maxAgeCache{$userPolicy} = $maxAge;
          }
        }

        if ((not defined $maxAge) || ($maxAge == 0)) {
          $log->info("No ppolicy max age defined for $cn");
          next;
        }

        if ($pwdChangedTimestamp + $maxAge <= $now) {
          $log->info("$cn is Expired");
          alert_user_if_needed($ldap,$entry,$now,'expired');
        } elsif ($pwdChangedTimestamp + $maxAge <= $next_alert_date) {
          alert_user_if_needed($ldap,$entry,$now,'ppolicy');
        }
      }
    }
  }

  if ($supann) {
    # SupAnn expiration
    # Consider supannRessourceEtatDate end date to be an expiration date if state is A
    my $mesg = $ldap->search(
      base    => $config->{'ldap_base'},
      filter  => '(&(objectClass=person)(supannRessourceEtatDate={COMPTE}A:*))',
      scope   => 'subtree'
    );
    die_on_ldap_errors($mesg);

    foreach my $entry ($mesg->entries()) {
      my $cn = $entry->get_value('cn');
      foreach my $supannRessourceEtatDate ($entry->get_value('supannRessourceEtatDate')) {
        my ($labelstate, $substate, $start, $end) = split(':', $supannRessourceEtatDate);
        if ($labelstate ne '{COMPTE}A') {
          next;
        }
        if ($end eq '') {
          next;
        }
        my $dt = DateTime->new(
          year  => substr($end, 0, 4),
          month => substr($end, 4, 2),
          day   => substr($end, 6, 2),
        );
        my $endInSeconds = $dt->epoch;
        if ($endInSeconds < $now) {
          $log->info("$cn has an invalid supannRessourceEtatDate value");
        } elsif ($endInSeconds <= $next_alert_date) {
          alert_user_if_needed($ldap,$entry,$now,'supann');
        }
      }
    }

    # Consider supannRessourceEtatDate start date to be an expiration date if state is
    $mesg = $ldap->search(
      base    => $config->{'ldap_base'},
      filter  => '(&(objectClass=person)(supannRessourceEtatDate={COMPTE}I:SupannExpire:*))',
      scope   => 'subtree'
    );
    die_on_ldap_errors($mesg);

    foreach my $entry ($mesg->entries()) {
      my $cn = $entry->get_value('cn');
      foreach my $supannRessourceEtatDate ($entry->get_value('supannRessourceEtatDate')) {
        my ($labelstate, $substate, $start, $end) = split(':', $supannRessourceEtatDate);
        if (($labelstate ne '{COMPTE}I') or ($substate ne 'SupannExpire')) {
          next;
        }
        if ($start eq '') {
          next;
        }
        my $dt = DateTime->new(
          year  => substr($start, 0, 4),
          month => substr($start, 4, 2),
          day   => substr($start, 6, 2),
        );
        my $startInSeconds = $dt->epoch;
        if ($startInSeconds > $now) {
          $log->info("$cn has an invalid supannRessourceEtatDate value");
        } else {
          alert_user_if_needed($ldap,$entry,$now,'expired');
        }
      }
    }
  }
}

sub get_mail_from_entry
{
  my ($entry) = @_;

  my $mail_address = $entry->get_value('mail');
  if ((not defined $mail_address) and $config->{'use_alternate'}) {
    $mail_address = $entry->get_value('gosaMailAlternateAddress');
  }
  if (not defined $mail_address) {
    $mail_address = $entry->get_value('supannAutreMail');
  }
  if (not defined $mail_address) {
    $mail_address = $entry->get_value('fdPrivateMail');
  }

  return $mail_address;
}

sub alert_user_if_needed
{
  my ($ldap,$entry,$now,$mode) = @_;

  if (($mode eq 'expired') and (not $config->{'alert_expired'})) {
    return;
  }

  my $cn = $entry->get_value('cn');
  my $forward_alert;
  if ($mode eq 'ppolicy') {
    $forward_alert = $config->{'forward_ppolicy'};
  } elsif ($mode eq 'expired') {
    $forward_alert = $config->{'forward_expired'};
  } else {
    $forward_alert = $config->{'forward_alert'};
  }

  # Check if we have a mail address for this user.
  my $mail_address = get_mail_from_entry($entry);
  if (not defined $mail_address) {
    $log->notice("User $cn has no mail address, skipping…");
    return;
  }

  my $first_email = 1;

  # Check if we already sent an email.
  my ($token_hash, $token_datetime) = get_ldap_token($ldap, $entry->get_value('uid'));
  if ($mode eq 'expired') {
    if ((defined $token_hash) && ($token_hash eq '{EXPIRED}')) {
      $log->info("User $cn was already sent a mail of expiration, not resending.");
      return;
    }
    undef $token_datetime;
  }
  if ((defined $token_datetime) && ($token_datetime + ($config->{'resend_delay'} * 86400) > $now)) {
    $log->info("User $cn was already sent a mail, not resending yet.");
    return;
  } elsif ((defined $token_hash) || (defined $token_datetime)) {
    # Delete obsolete token so we may create it again
    delete_ldap_token($ldap, $entry->get_value('uid'));
    $first_email = 0;
  }

  my ($manager_cn, $manager_mail);
  if ($forward_alert) {
    # Find the manager
    my $manager_dn = $entry->get_value('manager');
    if (not defined $manager_dn) {
      my $ou = $entry->dn;
      $ou =~ s/^[^,]+,$config->{'user_rdn'}//;
      my $manager_mesg = $ldap->search(
        base    => $ou,
        filter  => '(objectClass=*)',
        scope   => 'base'
      );
      if ($manager_mesg->count() > 0) {
        $manager_dn = ($manager_mesg->entries)[0]->get_value('manager');
      }
    }
    if (not defined $manager_dn) {
      $log->notice("No manager found for $cn");
    }
    my $manager_mesg = $ldap->search(
      base    => $manager_dn,
      filter  => '(objectClass=*)',
      scope   => 'base'
    );
    if ($manager_mesg->count() > 0) {
      $manager_cn   = ($manager_mesg->entries)[0]->get_value('cn');
      $manager_mail = get_mail_from_entry(($manager_mesg->entries)[0]);
    }
  }
  send_alert_mail($ldap, $entry->get_value('uid'), $now, $cn, $mail_address, $manager_cn, $manager_mail, $mode, $first_email);
}

sub send_alert_mail
{
  my ($ldap, $uid, $datetime, $user_cn, $user_mail, $manager_cn, $manager_mail, $mode, $first_email) = @_;
  my ($alert_mailsubject, $alert_mailbody, $token);
  if ($mode eq 'expired') {
    $alert_mailsubject  = $config->{'expired_mailsubject'};
    $alert_mailbody     = $config->{'expired_mailbody'};
    $token              = store_ldap_token($ldap, $uid, $datetime, 1);
  } elsif ($mode eq 'ppolicy') {
    $alert_mailsubject  = $config->{'ppolicy_mailsubject'};
    $alert_mailbody     = $config->{'ppolicy_mailbody'};
    $token              = '';
  } else {
    $alert_mailsubject  = $config->{'alert_mailsubject'};
    $alert_mailbody     = $config->{'alert_mailbody'};
    $token              = store_ldap_token($ldap, $uid, $datetime, 0);
  }
  if (($alert_mailbody eq '') || ($alert_mailsubject eq '')) {
    $log->info("Skipping mail to $user_cn<$user_mail> as mail body or subject is empty");
    return;
  }
  my $info_message = "Sending mail to $user_cn<$user_mail>";
  my $cc = "";
  if (defined $manager_mail) {
    $info_message .= ", copy to $manager_cn<$manager_mail>";
    $cc = encode_mimewords($manager_cn, Charset => 'utf-8', Encoding => 'B')." <$manager_mail>";
  }
  if ($token ne '') {
    $info_message .= " with token $token";
  }
  if ($mode ne 'expired') {
    if ($first_email) {
      $info_message .= " (first email)";
    } else {
      $info_message .= " (resent email)";
    }
  }
  $info_message .= " (from $mode)";
  $log->notice($info_message);
  my $body = sprintf($alert_mailbody,$user_cn,$uid,$token);
  my %message = (
    'From'                      => $config->{'alert_mailaddress'},
    'To'                        => encode_mimewords($user_cn, Charset => 'utf-8', Encoding => 'B')." <$user_mail>",
    'Cc'                        => $cc,
    'Subject'                   => encode_mimewords($alert_mailsubject, Charset => 'utf-8', Encoding => 'B'),
    'Content-type'              => 'text/plain; charset="utf-8"',
    'Content-Transfer-Encoding' => 'base64',
    'Message'                   => encode_base64($body)
  );
  sendmail(%message) or die $Mail::Sendmail::error;
}

sub get_ldap_token
{
  my ($ldap, $uid) = @_;

  my $dn = "ou=$uid,".$config->{'token_rdn'}.','.$config->{'fd_rdn'}.','.$config->{'ldap_base'};

  my $mesg = $ldap->search(
    base    => $dn,
    filter  => "(ou=$uid)",
    scope   => 'base'
  );

  if ($mesg->count()) {
    return (($mesg->entries)[0]->get_value('userPassword'), ($mesg->entries)[0]->get_value('description'));
  } else {
    return ();
  }
}

sub delete_ldap_token
{
  my ($ldap, $uid) = @_;

  my $dn = "ou=$uid,".$config->{'token_rdn'}.','.$config->{'fd_rdn'}.','.$config->{'ldap_base'};

  my $mesg = $ldap->delete($dn);
  $mesg->code && warn "! failed to delete token $dn: ".$mesg->error."\n";
}

sub store_ldap_token
{
  my ($ldap, $uid, $datetime, $expired) = @_;

  my $token_password;
  my $token_hash;
  if ($expired) {
    $token_password = '';
    $token_hash     = '{EXPIRED}';
  } else {
    $token_password  = argonaut_gen_random_str(48);
    $token_hash      = sha256_base64('expired'.$token_password);
    while (length($token_hash) % 4) {
      $token_hash .= '=';
    }
    $token_hash = "{SHA}".$token_hash;
  }

  my $dn = "ou=$uid,".$config->{'token_rdn'}.','.$config->{'fd_rdn'}.','.$config->{'ldap_base'};

  if (!argonaut_ldap_branch_exists($ldap, $config->{'token_rdn'}.','.$config->{'fd_rdn'}.','.$config->{'ldap_base'})) {
    die "! Branch ".$config->{'token_rdn'}.','.$config->{'fd_rdn'}.','.$config->{'ldap_base'}." doesnt exist \n";
  }

  my $mesg = $ldap->add(
    $dn,
    attr => [
      'ou'            => $uid,
      'objectClass'   => 'organizationalUnit',
      'userPassword'  => $token_hash,
      'description'   => $datetime
    ]
  );

  $mesg->code && die "! failed to create token $dn: ".$mesg->error."\n";

  return $token_password;
}

__END__

=head1 NAME

argonaut-user-reminder - read account expiration date from ldap and send emails reminders

=head1 SYNOPSIS

argonaut-user-reminder [--help] [--verbose] [--posix] [--ppolicy] [--supann] [--all (default)] [--supann-update]

=head1 DESCRIPTION

argonaut-user-reminder is a program used to read account expiration dates from the LDAP.
It reads the delay before expiration from the LDAP and send emails for user to postpone
expiration date or change the password.

=head1 OPTIONS

=over 3

=item B<--help>

Help message

=item B<--verbose>

Be verbose

=item B<--posix>

Check POSIX account expiration (based on shadowExpire)

=item B<--ppolicy>

Check ppolicy password expiration (based on pwdChangedTime and pwdMaxAge)

=item B<--supann>

Check supann expiration (based on supannRessourceEtatDate)

=item B<--all>

Check POSIX account, ppolicy password and supann expiration

=item B<--supann-update>

Check supannRessourceEtatDate end date and update values if needed.
To use this you need to fill [rest] section of argonaut.conf and install webservice plugin
 on your FusionDirectory installation.

=back

=head1 BUGS

Please report any bugs, or post any suggestions, to the fusiondirectory mailing list fusiondirectory-users or to
<https://gitlab.fusiondirectory.org/argonaut/argonaut/issues/new>

=head1 AUTHORS

Come Bernigaud

=head1 LICENCE AND COPYRIGHT

This code is part of Argonaut Project <https://www.argonaut-project.org/>

=over 1

=item Copyright (C) 2015-2018 FusionDirectory project

=back

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

=cut
