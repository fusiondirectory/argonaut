Argonaut AUTHORS
=======================

This is the alphabetical list of all people that have
contributed to the Argonaut project, beeing code, translations,
documentation and additional help.

* Benoit Mortier <benoit.mortier@fusiondirectory.org>
  Butracking, QA

* Antoine Gallavardin <antoine.gallavardin@free.fr>
  Quota original code

* Bernigaud Côme <come.bernigaud@fusiondirectory.org>
  All the new deployment stuff :)

* Samuel Bosquin <samuel.bosquin@ibcp.fr>
  Help and test with the RPM code and FAI
  
* Jonathan Swaelens <jonathan.swaelens@fusiondirectory.org>
  QA, Testing, Systemd units  

* Sean Thatcher papertray3@gmail.com
  Corrected Argonaut systemd units

* Thomas Niercke <thomas@niercke.de>
  Code for the samba share argonaut module
  
